
#ifndef SNMP_EVENT_H_
#define SNMP_EVENT_H_

#include <stdio.h>
#include <pthread.h>
#include <signal.h>


#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include <net-snmp/library/parse.h>


#define OID_BASE (13)

#define ASC_DVB_EVENT 22
#define ASC_CHANNEL_EVENT 23

#define OID_TABLE_INDEX (OID_BASE + 1)
#define OID_ROW_INDEX (OID_BASE + 2)
#define OID_COLUMN_INDEX OID_BASE

#define ASC_OID_LENGTH (OID_BASE + 3)


#define ASC_TIMESTAMP 1
#define ASC_DVB_ID 2
#define ASC_STATUS 3
#define ASC_SIGNAL 4
#define ASC_SNR 5
#define ASC_BER 6
#define ASC_UNC 7

#define ASC_CHANNEL_ID 2
#define ASC_INPUT_ID 3
#define ASC_ONAIR 4
#define ASC_BITRATE 5
#define ASC_SCRAMBLED 6
#define ASC_CC_ERROR 7
#define ASC_PES_ERROR 8

typedef struct asc_data_dvb_event_s {
		struct asc_data_dvb_event_s * next;
		char * dvb;
		size_t dvb_len;
		time_t timestamp;
		int status;
		int signal;
		int snr;
		int ber;
		int unc;
} asc_data_dvb_event;

typedef struct asc_data_channel_event_s {
		struct asc_data_channel_event_s * next;
		char * channel;
		size_t channel_len;
		time_t timestamp;
		int input;
		int onair;
		int bitrate;
		int scrambled;
		int cc_error;
		int pes_error;
} asc_data_channel_event;

extern void asc_init_snmp_event(int instance);

extern void asc_loop_requests();

extern int asc_update_dvb_event_row(const char * dvb,  time_t timestamp, int status, int signal, int snr, int ber, int unc);
extern int asc_update_channel_event_row(const char * channel, int input, time_t timestamp, int onair, int bitrate, int scrambled, int cc, int pes);

#endif /* SNMP_EVENT_H_ */
