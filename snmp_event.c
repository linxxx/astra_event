#include "snmp_event.h"

const char * ASC_DVB_EVENT_TABLE = "astraDvbEventTable";
const char * ASC_CHANNEL_EVENT_TABLE = "astraChannelEventTable";


oid asc_adpter_event_table_oid[] =   { 1, 3, 6, 1, 4, 1, 37490, 9000, 10, 1, 1, 1, 1, 1, 0};
oid asc_channel_event_table_oid[] = { 1, 3, 6, 1, 4, 1, 37490, 9000, 10, 1, 1, 2, 1, 1, 0};

static asc_data_dvb_event * dvb_row  = NULL;
static asc_data_channel_event * channel_row  = NULL;

static netsnmp_handler_registration * dvb_handler = NULL;
static netsnmp_handler_registration * channel_handler = NULL;


static int snmp_subagent_running;

const char * AGENT_NAME = "astra";

RETSIGTYPE stop_server(int a) {
	snmp_subagent_running = 0;
}

void * asc_find_row(int index, int type)
{
	int cnt = 1;
	if (type == ASC_DVB_EVENT)
	{
		asc_data_dvb_event * row;
		for (row = dvb_row; row; row = row->next, cnt++)
		{
			if (cnt == index)
				return row;
		}
	}
	else if (type == ASC_CHANNEL_EVENT)
	{
		asc_data_channel_event * row;
		for (row = channel_row; row; row = row->next, cnt++)
		{
			if (cnt == index)
				return row;
		}
	}
	return NULL;
}

static int dvbEventTableHandler (
		netsnmp_mib_handler * handler,
		netsnmp_handler_registration * reginfo,
		netsnmp_agent_request_info * reqinfo,
		netsnmp_request_info * requests)
{
	netsnmp_request_info *request;

	oid tmp_reqoid[MAX_OID_LEN];
	int column;
	asc_data_dvb_event * row;
	int index;
	for (request = requests; request; request = request->next)
	{
		if (request->processed)
			continue;

		switch (reqinfo->mode)
		{
			case MODE_GET:
				if (request->requestvb->name_length < OID_ROW_INDEX)
				{
					return SNMP_NOSUCHINSTANCE;
				}

				index = (request->requestvb->name_length < ASC_OID_LENGTH) ? 1 : request->requestvb->name[OID_ROW_INDEX] ;
				row = asc_find_row(index, ASC_DVB_EVENT);
				if (row == NULL)
				{
					return SNMP_NOSUCHINSTANCE;
				}

				memcpy(tmp_reqoid, reginfo->rootoid, (reginfo->rootoid_len) * sizeof(oid));
				tmp_reqoid[OID_ROW_INDEX] = index;
				snmp_set_var_objid(request->requestvb, tmp_reqoid, ASC_OID_LENGTH);

				break;
			case MODE_GETNEXT:
			case MODE_GETBULK:
				// Find next row item
				if (snmp_oid_compare(reginfo->rootoid, reginfo->rootoid_len,  request->requestvb->name, reginfo->rootoid_len) != 0)
				{
					index = 1;
					row = asc_find_row(index, ASC_DVB_EVENT);
					if (row == NULL)
					{
						return SNMP_NOSUCHINSTANCE;
					}

					memcpy(tmp_reqoid, reginfo->rootoid, reginfo->rootoid_len * sizeof(oid));
					tmp_reqoid[OID_ROW_INDEX]  = index;
					snmp_set_var_objid(request->requestvb, tmp_reqoid, reginfo->rootoid_len + 1);
					snmp_set_var_typed_value(	request->requestvb, ASN_NULL, (u_char *)NULL, 0);
					break;
				}
				else
				{
					index = request->requestvb->name[OID_ROW_INDEX] + 1;
					row = asc_find_row(index, ASC_DVB_EVENT);
					memcpy(tmp_reqoid, reginfo->rootoid, reginfo->rootoid_len * sizeof(oid));
					tmp_reqoid[OID_ROW_INDEX] = index;
					snmp_set_var_objid(request->requestvb, tmp_reqoid, reginfo->rootoid_len + 1);
					if (row == NULL)
					{
						snmp_set_var_typed_value(	request->requestvb, ASN_NULL, (u_char *)NULL, 0);
						return SNMP_ERR_NOERROR;
					}
				}
				break;
			default:
				snmp_log(LOG_ERR, "Unsupported mode %i\n", reqinfo->mode);
				return SNMP_ERR_GENERR;
		}

		column = reginfo->rootoid[OID_COLUMN_INDEX];

		switch(column)
		{
			case ASC_TIMESTAMP:
				snmp_set_var_typed_value(request->requestvb, ASN_UNSIGNED, (u_char *)&(row->timestamp), sizeof(time_t));
				break;
			case ASC_STATUS:
				snmp_set_var_typed_value(request->requestvb, ASN_INTEGER, (u_char *)&(row->status), sizeof(int));
				break;
			case ASC_SIGNAL:
				snmp_set_var_typed_value(request->requestvb, ASN_INTEGER, (u_char *)&(row->signal), sizeof(int));
				break;
			case ASC_SNR:
				snmp_set_var_typed_value(request->requestvb, ASN_INTEGER, (u_char *)&(row->snr), sizeof(int));
				break;
			case ASC_BER:
				snmp_set_var_typed_value(request->requestvb, ASN_INTEGER, (u_char *)&(row->ber), sizeof(int));
				break;
			case ASC_UNC:
				snmp_set_var_typed_value(request->requestvb, ASN_INTEGER, (u_char *)&(row->unc), sizeof(int));
				break;
			case ASC_DVB_ID:
				snmp_set_var_typed_value(request->requestvb, ASN_OCTET_STR, (u_char *)row->dvb, row->dvb_len);
				break;
			default:
				return SNMP_ERR_GENERR;
		}
	}
	return SNMP_ERR_NOERROR;
}


static int channelEventTableHandler(
		netsnmp_mib_handler * handler,
		netsnmp_handler_registration * reginfo,
		netsnmp_agent_request_info * reqinfo,
		netsnmp_request_info * requests)
{
	netsnmp_request_info *request;
	oid tmp_reqoid[MAX_OID_LEN];
	int column;
	asc_data_channel_event * row;
	int index;

	for (request = requests; request; request = request->next)
	{
		if (request->processed)
			continue;

		switch (reqinfo->mode)
		{
			case MODE_GET:
				if (request->requestvb->name_length < OID_ROW_INDEX)
				{
					return SNMP_NOSUCHINSTANCE;
				}

				index = (request->requestvb->name_length < ASC_OID_LENGTH) ? 1 : request->requestvb->name[OID_ROW_INDEX] ;
				row = asc_find_row(index, ASC_CHANNEL_EVENT);
				if (row == NULL)
				{
					return SNMP_NOSUCHINSTANCE;
				}

				memcpy(tmp_reqoid, reginfo->rootoid, (reginfo->rootoid_len) * sizeof(oid));
				tmp_reqoid[OID_ROW_INDEX] = index;
				snmp_set_var_objid(request->requestvb, tmp_reqoid, ASC_OID_LENGTH);
				break;
			case MODE_GETNEXT:
			case MODE_GETBULK:
				// Find next row item
				if (snmp_oid_compare(reginfo->rootoid, reginfo->rootoid_len,  request->requestvb->name, reginfo->rootoid_len) != 0)
				{
					index = 1;
					row = asc_find_row(index, ASC_CHANNEL_EVENT);
					if (row == NULL)
					{
						return SNMP_NOSUCHINSTANCE;
					}

					memcpy(tmp_reqoid, reginfo->rootoid, reginfo->rootoid_len * sizeof(oid));
					tmp_reqoid[OID_ROW_INDEX]  = index;
					snmp_set_var_objid(request->requestvb, tmp_reqoid, reginfo->rootoid_len + 1);
					snmp_set_var_typed_value(	request->requestvb, ASN_NULL, (u_char *)NULL, 0);
					request->repeat = 0;
					break;
				}
				else
				{
					index = request->requestvb->name[OID_ROW_INDEX] + 1;
					row = asc_find_row(index, ASC_CHANNEL_EVENT);
					memcpy(tmp_reqoid, reginfo->rootoid, reginfo->rootoid_len * sizeof(oid));
					tmp_reqoid[OID_ROW_INDEX] = index;
					snmp_set_var_objid(request->requestvb, tmp_reqoid, reginfo->rootoid_len + 1);
					if (row == NULL)
					{
						snmp_set_var_typed_value(	request->requestvb, ASN_NULL, (u_char *)NULL, 0);
						return SNMP_ERR_NOERROR;
					}
				}
				break;
			default:
				snmp_log(LOG_ERR, "Unsupported mode %i\n", reqinfo->mode);
				return SNMP_ERR_GENERR;
		}

		column = reginfo->rootoid[OID_COLUMN_INDEX];

		switch(column)
		{
			case ASC_TIMESTAMP:
				snmp_set_var_typed_value(request->requestvb, ASN_UNSIGNED, (u_char *)&(row->timestamp), sizeof(time_t));
				break;
			case ASC_INPUT_ID:
				snmp_set_var_typed_value(request->requestvb, ASN_INTEGER, (u_char *)&(row->input), sizeof(int));
				break;
			case ASC_ONAIR:
				snmp_set_var_typed_value(request->requestvb, ASN_INTEGER, (u_char *)&(row->onair), sizeof(int));
				break;
			case ASC_BITRATE:
				snmp_set_var_typed_value(request->requestvb, ASN_INTEGER, (u_char *)&(row->bitrate), sizeof(int));
				break;
			case ASC_SCRAMBLED:
				snmp_set_var_typed_value(request->requestvb, ASN_INTEGER, (u_char *)&(row->scrambled), sizeof(int));
				break;
			case ASC_CC_ERROR:
				snmp_set_var_typed_value(request->requestvb, ASN_INTEGER, (u_char *)&(row->cc_error), sizeof(int));
				break;
			case ASC_PES_ERROR:
				snmp_set_var_typed_value(request->requestvb, ASN_INTEGER, (u_char *)&(row->pes_error), sizeof(int));
				break;
			case ASC_DVB_ID:
				snmp_set_var_typed_value(request->requestvb, ASN_OCTET_STR, (u_char *)row->channel, row->channel_len);
				break;
			default:
				return SNMP_ERR_GENERR;
		}
	}
	return SNMP_ERR_NOERROR;
}

int asc_update_dvb_event_row(const char * dvb,  time_t timestamp, int status, int signal, int snr, int ber, int unc)
{
	if (dvb == NULL)
		return 1;

	int dvb_len = strlen(dvb);
	if (dvb_len < 1)
		return 1;

	asc_data_dvb_event * row;
	for (row = dvb_row; row; row = row->next)
	{
		if (strcmp(row->dvb, dvb) == 0)
			break;
	}

	// If not found dvb event row then create new row
	if (row  == NULL)
	{
		row = SNMP_MALLOC_TYPEDEF(asc_data_dvb_event);
		if (row == NULL)
			return 1;

	    row->dvb_len = dvb_len;

	    row->dvb = strndup(dvb, row->dvb_len);
	    if (dvb_row == NULL)
	    {
	    	dvb_row = row;
	    }
	    else
	    {
	    	asc_data_dvb_event * last_row;

	    	for (last_row = dvb_row; last_row->next;  last_row = last_row->next) { }
	    	last_row->next = row;
	    }
	}

    row->timestamp = timestamp;
    row->status = status;
    row->signal = signal;
    row->snr = snr;
    row->ber = ber;
    row->unc = unc;

    return 0;
}

int asc_update_channel_event_row(const char * channel, int input, time_t timestamp, int onair, int bitrate, int scrambled, int cc, int pes)
{
	if (channel == NULL)
		return 1;

	int channel_len = strlen(channel);

	if (channel_len < 1)
		return 1;

	asc_data_channel_event * row;
	for (row = channel_row; row; row = row->next)
	{
		if (strcmp(row->channel, channel) == 0)
			break;
	}
	// If not found dvb event row then create new row
	if (row  == NULL)
	{
		row = SNMP_MALLOC_TYPEDEF(asc_data_channel_event);
		if (row == NULL)
			return 1;

		row->channel_len = channel_len;
		row->channel = strndup(channel, row->channel_len);
		if (channel_row == NULL)
		{
			channel_row = row;
		}
		else
		{
			asc_data_channel_event * last_row;
	    	for (last_row = channel_row; last_row->next;  last_row = last_row->next) { }
	    	last_row->next = row;
		}
	}

	row->input = input;
	row->timestamp = timestamp;
	row->onair = onair;
	row->bitrate = bitrate;
	row->scrambled = scrambled;
	row->cc_error = cc;
	row->pes_error = pes;
	return 0;
}



void asc_init_snmp_event(int instance)
{
	netsnmp_ds_set_boolean(NETSNMP_DS_APPLICATION_ID, NETSNMP_DS_AGENT_ROLE, 1);
	snmp_enable_stderrlog();
	init_agent(AGENT_NAME);
	debug_register_tokens("handler");
	debug_register_tokens("results");
	debug_register_tokens("table_data");

	snmp_set_do_debugging(1);
	asc_adpter_event_table_oid[OID_TABLE_INDEX] = instance;
	asc_channel_event_table_oid[OID_TABLE_INDEX] = instance;

	dvb_handler = netsnmp_create_handler_registration(
				ASC_DVB_EVENT_TABLE, dvbEventTableHandler,
				asc_adpter_event_table_oid,
				OID_LENGTH(asc_adpter_event_table_oid),
				HANDLER_CAN_DEFAULT | HANDLER_CAN_GETANDGETNEXT
	);

	dvb_handler->range_subid = OID_LENGTH(asc_adpter_event_table_oid) - 1;
	dvb_handler->range_ubound = (oid)7;
    netsnmp_register_handler(dvb_handler);

	channel_handler = netsnmp_create_handler_registration(
				ASC_CHANNEL_EVENT_TABLE, channelEventTableHandler,
				asc_channel_event_table_oid,
				OID_LENGTH(asc_channel_event_table_oid),
				HANDLER_CAN_DEFAULT | HANDLER_CAN_GETANDGETNEXT
	);

	channel_handler->range_subid = OID_LENGTH(asc_channel_event_table_oid) - 1;
	channel_handler->range_ubound = (oid)8;
    netsnmp_register_handler(channel_handler);
}



void free_handler_registration(netsnmp_handler_registration * handler)
{
	if (handler)
	{
		if (handler->handler)
			netsnmp_handler_free(handler->handler);

		SNMP_FREE(handler->handlerName);
		SNMP_FREE(handler->rootoid);
		free(handler);
	}
}

void asc_free_row(int type)
{
	if (type == ASC_DVB_EVENT)
	{
		asc_data_dvb_event * row, * row_next;
		for (row = dvb_row; row;)
		{
			row_next = row->next;
			if (row->dvb)
				free(row->dvb);
			free(row);
			row = row_next;
		}
		dvb_row = NULL;
	}
	else if (type == ASC_CHANNEL_EVENT)
	{
		asc_data_channel_event * row, * row_next;
		for (row = channel_row; row;)
		{
			row_next = row->next;
			if (row->channel)
				free(row->channel);
			free(row);
			row = row_next;
		}
		channel_row = NULL;
	}
}

void asc_loop_requests()
{
	init_snmp(AGENT_NAME);

	/* In case we recevie a request to stop (kill -TERM or kill -INT) */
	snmp_subagent_running = 1;
	signal(SIGTERM, stop_server);
	signal(SIGINT, stop_server);

	while(snmp_subagent_running)
	{
		agent_check_and_process(1); /* 0 == don't block */
	}

	free_handler_registration(dvb_handler);
	free_handler_registration(channel_handler);
	asc_free_row(ASC_DVB_EVENT);
	asc_free_row(ASC_CHANNEL_EVENT);
	snmp_shutdown(AGENT_NAME);
}

int main(int argc, char ** argv)
{

	asc_init_snmp_event(34679);
    const char * dvb0 = "dvb_0";
    const char * dvb1 = "dvb_1";
    asc_update_dvb_event_row(dvb0, 3537637000,46, 15, 44, 55, 64);
    asc_update_dvb_event_row(dvb1, 537567,4, 31, 34, 23, 65);


    const char * channel0 = "channel_0";
    const char * channel1 = "channel_1";

    asc_update_channel_event_row(channel0, 1, 3537637000,1, 15535, 1, 545, 94);
    asc_update_channel_event_row(channel1, 1, 3637637000,1, 4348, 1, 655, 364);

    asc_loop_requests();

	return 0;
}
