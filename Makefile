NET_SNMP_CFLAGS = $(shell net-snmp-config --cflags)
NET_SNMP_LDLAGS = $(shell net-snmp-config --agent-libs)


LDFLAGS= -lpthread $(NET_SNMP_LDLAGS)

all:
	rm -f *.o astra_event
	gcc -c -o snmp_event.o snmp_event.c -fPIC -Wall $(NET_SNMP_CFLAGS)
	gcc -o astra_event snmp_event.o $(LDFLAGS)
clean:
	rm -f *.o astra_event
